#!/usr/bin/env python3.6
import sys
import os
import requests

def compare(comparator, staging, prod):
    if staging > prod:
        print(f'Improvement in {comparator}')
    elif prod == staging:
        print(f'No change in {comparator}')
    else:
        print(f'Degradation in {comparator}')
        print(f'{prod} -> {staging}')
        if os.getenv(f'LIGHTHOUSE_EXCEPTION_{comparator.upper()}'):
            print(f'Ignoring a {comparator} degradation from {prod} to {staging}')
        else:
            sys.exit(1)

STAGING_CAT = requests.get('https://staging.andrewjohnperry.com/lighthouse.json').json()['categories']
PRODUCTION_CAT = requests.get('https://andrewjohnperry.com/lighthouse.json').json()['categories']

for category in ['performance', 'accessibility', 'best-practices', 'seo', 'pwa']:
    compare(category, STAGING_CAT[category]['score'], PRODUCTION_CAT[category]['score'])
