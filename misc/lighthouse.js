const fetch = require("node-fetch");
const fs = require("fs");
const getData = async url => {
  try {
    const response = await fetch(url);
    return await response.json();
  } catch (error) {
    console.log(error);
  }
};

function compare(comparator, staging, prod) {
  if (staging > prod) {
    console.log(`Improvement in ${comparator}`);
  } else if (prod == staging) {
    console.log(`No change in ${comparator}`);
  } else {
    console.log(`Degradation in ${comparator}`);
    console.log(`${prod} -> ${staging}`);
    if (process.env[`LIGHTHOUSE_EXCEPTION_${comparator.toUpperCase()}`]) {
      console.log(
        `Ignoring a ${comparator} degradation from ${prod} to ${staging}`
      );
    } else {
      process.exit(1);
    }
  }
}

async function grabFiles(staging_data, prod_url) {
  try {
    // console.log(staging_data);
    const prod_data = await getData(prod_url);
    ["performance", "accessibility", "best-practices", "seo", "pwa"].forEach(
      element => {
        compare(
          element,
          staging_data.categories[element].score,
          prod_data.categories[element].score
        );
      }
    );
  } catch (err) {
    console.log(err);
  }
}

function main(lighthouse_file) {
  const prod_url = "https://andrewjohnperry.com/lighthouse.report.json";
  fs.readFile(lighthouse_file, (err, data) => {
    if (err) throw err;
    grabFiles(JSON.parse(data), prod_url);
  });
}

main(process.argv[2]);
