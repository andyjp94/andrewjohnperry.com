# Yay! You want to progress to production!
Unfortunately before you continue it is important to ensure that all manual checks are complete.

## Accessibility
Unfortunately a lot of accessibility verification is manual. If you aren't comfortable completing the following checklist then please read this [guide](https://developers.google.com/web/fundamentals/accessibility/how-to-review?utm_source=lighthouse&utm_medium=cli). It should be noted that we are mainly aiming to avoid regression, it is not a requirements to fulfill all of the criteria, it is just necessary to avoid regression and where possible to improve.

| Checklist | Verified |
| ------ | ------ |
| This page has a logical tab order |  |
| Interactive controls are keyboard focusable |  |
| Interactive elements indicate their purpose and state | |
| The user's focus is directed to new content added to the page | |
| User focus is not accidentally trapped in a region |  |
| Custom controls have associated labels |  |
| Custom controls have ARIA roles | |
| Visual order on the page follows DOM order | |
| Offscreen content is hidden from assistive technology | |
| Headings don't skip levels | |
| HTML5 landmark elements are used to improve navigation | |

## Search Engine Optimisation
This can be checked using this [tool](https://search.google.com/structured-data/testing-tool/u/0/). For further reading on this subject please read this [guide](https://developers.google.com/search/docs/guides/prototype)

| Checklist | Verified |
| ------ | ------ |
|  Structured data is valid |  |

## Progressive Web App
These are straight forward. If you aren't familiar with what a progressive web app is then please read this [guide](https://developers.google.com/web/progressive-web-apps/)

| Checklist | Verified |
| ------ | ------ |
| Site works cross-browser |  |
| Page transitions don't feel like they block on the network |  |
| Each page has a URL | |
