const correctAnswer = [
  {
    "0": "1",
    "1": "2",
    "2": "7",
    "3": "5",
    "4": "4",
    "5": "9",
    "6": "3",
    "7": "6",
    "8": "8"
  },
  {
    "0": "8",
    "1": "5",
    "2": "6",
    "3": "7",
    "4": "2",
    "5": "3",
    "6": "9",
    "7": "4",
    "8": "1"
  },
  {
    "0": "3",
    "1": "4",
    "2": "9",
    "3": "8",
    "4": "6",
    "5": "1",
    "6": "5",
    "7": "7",
    "8": "2"
  },
  {
    "0": "6",
    "1": "7",
    "2": "2",
    "3": "9",
    "4": "1",
    "5": "5",
    "6": "8",
    "7": "3",
    "8": "4"
  },
  {
    "0": "5",
    "1": "8",
    "2": "3",
    "3": "4",
    "4": "7",
    "5": "2",
    "6": "1",
    "7": "9",
    "8": "6"
  },
  {
    "0": "4",
    "1": "9",
    "2": "1",
    "3": "3",
    "4": "8",
    "5": "6",
    "6": "2",
    "7": "5",
    "8": "7"
  },
  {
    "0": "9",
    "1": "1",
    "2": "4",
    "3": "6",
    "4": "5",
    "5": "8",
    "6": "7",
    "7": "2",
    "8": "3"
  },
  {
    "0": "2",
    "1": "6",
    "2": "5",
    "3": "1",
    "4": "3",
    "5": "7",
    "6": "4",
    "7": "8",
    "8": "9"
  },
  {
    "0": "7",
    "1": "3",
    "2": "8",
    "3": "2",
    "4": "9",
    "5": "4",
    "6": "6",
    "7": "1",
    "8": "5"
  }
];

// Helper function so that the sudoku isn't being referenced but truly copied
const clone = x => {
  let newArray = [];
  x.forEach(obj => {
    newArray.push({ ...obj });
  });
  return newArray;
};

import Sudoku from "@/js/sudoku";

test("expect empty cells to be 3", () => {
  const emptyAnswer = clone(correctAnswer);

  emptyAnswer[0]["0"] = "0";
  emptyAnswer[7]["0"] = "";
  emptyAnswer[3]["5"] = "789";
  let s = new Sudoku(emptyAnswer, correctAnswer);
  expect(s.empty).toBe(3);
});

test("expect empty cells to be 0", () => {
  const s = new Sudoku(correctAnswer, correctAnswer);
  expect(s.empty).toBe(0);
});

test("expect wrong cells to be 1", () => {
  const wrongAnswer = clone(correctAnswer);
  wrongAnswer[0]["0"] = (10 - wrongAnswer[0][0]).toString();
  const s = new Sudoku(wrongAnswer, correctAnswer);
  expect(s.wrong).toBe(1);
});

test("expect wrong cells to be 0", () => {
  const wrongAnswer = clone(correctAnswer);
  const s = new Sudoku(wrongAnswer, correctAnswer);
  expect(s.wrong).toBe(0);
});

test("expect complete to be false when cells are empty", () => {
  const completeAnswer = clone(correctAnswer);
  completeAnswer[5]["7"] = "0";
  completeAnswer[3]["8"] = "0";

  const s = new Sudoku(completeAnswer, correctAnswer);
  expect(s.complete).toBe(false);
});

test("expect complete to be true when cells are full", () => {
  const s = new Sudoku(correctAnswer, correctAnswer);
  expect(s.complete).toBe(true);
});

test("expect reset to set problem to original", () => {
  const wrongAnswer = clone(correctAnswer);
  wrongAnswer[0]["0"] = (10 - wrongAnswer[0][0]).toString();
  const s = new Sudoku(correctAnswer, correctAnswer);
  s.problem = wrongAnswer;
  expect(s.problem).toEqual(wrongAnswer);
  s.reset();
  expect(s.problem).toEqual(correctAnswer);
});
